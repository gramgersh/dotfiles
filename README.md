# dotfiles

This repo contains dot files and bin/lib/etc files for use across all
Linux systems.

To install:

```bash
cd $HOME
curl -s https://bitbucket.org/gramgersh/dotfiles/get/master.tar.gz | \
	tar xzf - --wildcards --strip-components=2 --suffix=.$(date "+%Y%m%d-%H%M") */home
```

