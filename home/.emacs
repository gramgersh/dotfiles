(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auto-compression-mode t nil (jka-compr))
 '(case-fold-search t)
 '(current-language-environment "UTF-8")
 '(default-input-method "rfc1345")
 '(global-font-lock-mode t nil (font-lock))
 '(inhibit-startup-screen t)
 '(line-number-mode t)
 '(tab-width 4)
 '(text-mode-hook (quote (viper-mode turn-on-auto-fill text-mode-hook-identify)))
 '(tool-bar-mode nil nil (tool-bar))
 '(toolbar-visible-p nil)
 '(truncate-lines nil)
 '(user-mail-address "erickson@scottywotty.com")
 '(vc-handled-backends (quote (RCS)))
 '(viper-auto-indent t)
 '(viper-mode t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:stipple nil :background "white" :foreground "black" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 140 :width normal :family "adobe-courier"))))
 '(bold-italic ((t (:size "16" :bold t :italic t))))
 '(cperl-hash-face ((((class color) (background light)) (:foreground "Red" :bold t))))
 '(font-lock-comment-face ((((class color) (background light)) (:foreground "blue4" :background "WhiteSmoke"))))
 '(font-lock-doc-string-face ((((class color) (background light)) (:foreground "green4" :background "WhiteSmoke"))))
 '(font-lock-function-name-face ((((class color) (background light)) (:foreground "red3" :background "WhiteSmoke"))))
 '(font-lock-keyword-face ((((class color) (background light)) (:foreground "blue3" :background "WhiteSmoke"))))
 '(font-lock-string-face ((((class color) (background light)) (:foreground "green4" :background "WhiteSmoke"))))
 '(font-lock-type-face ((((class color) (background light)) (:foreground "blue3" :background "WhiteSmoke"))))
 '(font-lock-variable-name-face ((((class color) (background light)) (:foreground "OrangeRed" :background "WhiteSmoke")))))
(global-set-key (kbd "<M-p>") 'mouse-yank-primary)
(viper-mode)
(server-start)
