# This file is provided by:
#
#   https://bitbucket.org/gramgersh/dotfiles/
#
# Environment Variables
#
# This shouldn't happen.
[[ -z "$PATH" ]] && PATH=/bin:/usr/bin:/sbin:/usr/sbin

# Set up the "erickson" user.  May be different on each system.
if [ -z "$E_USER" ]; then
	[ -f $HOME/.erickson_user ] && read E_USER < $HOME/.erickson_user
	[ -z "$E_USER" ] && E_USER=erickson
fi

if [ -z "$E_HOME" ]; then
	[ -n "$E_USER" ] && E_HOME=$(getent passwd $E_USER | awk -F: '{ print $6 }')
fi

# If no USER set, set via the id command
[[ -z "$USER" ]] && USER=$(id -u -n)

# Source the global definitions only if this is the erickson user
[[ "$USER" == "$E_USER" && -f /etc/bashrc ]] && . /etc/bashrc

[[ -z "$HOSTNAME" ]] && HOSTNAME=$(uname -n)
[[ -z "$SHORTHOST" ]] && SHORTHOST=${HOSTNAME%%.*}

##
## FUNCTIONS
##

# Grab the general functions also available to scripts:
[[ -n "$E_HOME" && -f $E_HOME/etc/erickson.functions ]] && . $E_HOME/etc/erickson.functions

# Start up an xterm, setting the -name option to the name of the
# running user.
function myterm {
	xterm -ls -name ${USER}xterm "$@" &
}

# For the $E_USER, create an 'xsudo' alias that runs sudo after
# setting the GSUXAUTH envar
if [[ "$USER" == "$E_USER" ]] ; then
	# xsudo user
	function xsudo {
		typeset _TMPDISPLAY
		typeset _newuser
		_newuser=${1:-root}
		if [[ -z "$DISPLAY" ]]; then
			echo "No DISPLAY set"
			return
		fi
		if type -p xauth >/dev/null ; then
			GSUXAUTH=$(xauth -i nlist $DISPLAY)
			if [[ -z "$GSUXAUTH" ]]; then
				_TMPDISPLAY=$(echo "$DISPLAY" | sed -e 's/^localhost//' -e 's/\.0//')
				GSUXAUTH=$(xauth -i nlist $_TMPDISPLAY)
			fi
			export GSUXAUTH
		fi
		(xterm -ls -name ${_newuser}xterm -title ${_newuser}@${SHORTHOST} -n ${_newuser}@${SHORTHOST} -e sudo $SUDO_ARGS -s -H -u $_newuser &)
	}
fi

function xswt {
	unset PROMPT_COMMAND
	echo -ne "\e0;$@\a"
}


##
## END FUNCTIONS
##

##
## ALIASES
##
alias ec='emacsclient --no-wait'
alias j='jobs -l'
alias ll='ls -lh'
alias ls='/bin/ls -F'
alias nil='/bin/cat >/dev/null'
alias pd=pushd
alias pg="ps -ef|grep"
alias rs='printf "\x1Bc";eval `resize`'
alias xless="xprog less"
alias xman="xprog man"
alias xperldoc="xprog perldoc -o text"
alias xtelnet="xprog telnet -- -name bluexterm"
alias xvi="xprog vi"
alias xvim="xprog vim"

# Don't actually want these aliased.  Some times it is aliased by the
# system to add color, but some times not.  So alias it, then unalias it.
alias vi=vim ; unalias vi
alias egrep=grep ; unalias egrep
alias fgrep=grep ; unalias fgrep
alias grep=egrep ; unalias grep

# Use vim for gpg files.  Don't let it try to open an X11 password
# program.
alias vim='DISPLAY= /usr/bin/vim'

# Alias xroot only for the E_USER
if [[ "$USER" = "$E_USER" ]]; then
    alias xroot="xsudo root"
fi

##
## END ALIASES
##

# notify of background jobs
set -o notify
# Use the emacs-style line editing
set -o emacs
# Don't logout on ^D
set -o ignoreeof

# For cygwin, don't worry about case
case "`uname`" in
	CYGWIN* )
		shopt -s nocaseglob
		;;
esac

# Don't put duplicate lines in the history.
export HISTCONTROL="ignoredups"

# Ignore some controlling instructions
export HISTIGNORE="[   ]*:&:bg:fg:exit"

export EXINIT='set ai wm=10 sw=4 showmatch hls!'
export GPG_TTY=$(tty)
export LC_ALL=C
PS1='\h:\w($?)\$ '
export SUDO_PS1='\u@\h:\w($?)\$ '
export RSYNC_RSH=ssh

# Don't care about local mail.
unset MAIL
unset MAILCHECK

case "$TERM" in
	xterms | xterm-* ) TERM=xterm ;;
esac

[[ -z "$SVN_EDITOR" ]] && SVN_EDITOR=vi
export SVN_EDITOR

if [[ "$PS1" ]]; then
    case $TERM in
        xterm*) PROMPT_COMMAND='printf "\e]0;${USER}@${SHORTHOST}:${PWD/#$HOME/~}\a";' ;;
        screen) PROMPT_COMMAND='printf "\e]0;_${USER}@${SHORTHOST}:${PWD/#$HOME/~}\a"' ;;
    esac
fi

# Do host- and site-specific startups
_senv_local .bashrc

# If we're not E_USER, set the xauth data
[[ "$USER" != "$E_USER" ]] && setxauth
