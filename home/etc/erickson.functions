# This file is provided by:
#
#   https://bitbucket.org/gramgersh/dotfiles/
#
# Environment Variables
#
[[ -z "$HOSTNAME" ]] && HOSTNAME=$(uname -n)
[[ -z "$SHORTHOST" ]] && SHORTHOST=${HOSTNAME%%.*}
if [[ -z "$E_USER" ]]; then
    [ -f $HOME/.erickson_user ] && read E_USER < $HOME/.erickson_user
    [ -z "$E_USER" ] && E_USER=erickson
fi

if [ -z "$E_HOME" ]; then
    [ -n "$E_USER" ] && E_HOME=$(getent passwd $E_USER | awk -F: '{ print $6 }')
fi

# Find 'localized' versions of files with the base of BASE, and set them
# into the $_E_FLF array variable.
#
# The suffixes appended are:
#     .local
#     .$SHORTHOST
#     .$HOSTNAME
#     .$USER
#
# Append (-s) or prepend (-S) a suffix to the list
#
# function _find_local_files [-s SUFFIX] [-S SUFFIX] BASE [BASE2 ...]
function _find_local_files {
	typeset _BASE
	typeset _SUFFIXES
	typeset _FILE
	typeset _SUFFIX

	# Set up the standard suffixes.
	_SUFFIXES=(local)
	[[ -n "${SHORTHOST}" ]] && _SUFFIXES+=( $SHORTHOST )
	[[ -n "${HOSTNAME}" && "${SHORTHOST}" != "${HOSTNAME}" ]] && _SUFFIXES+=( $HOSTNAME )
	[[ "$USER" != "$E_USER" ]] && _SUFFIXES+=( $USER )

	# See if there were any more suffixes
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-s ) _SUFFIXES+=( "$2" ) ; shift 2 ;;
			-S ) _SUFFIXES=( "$2" "${_SUFFIXES[@]}" ) ; shift 2 ;;
			* ) break ;;
		esac
	done

	# Go through each of the bases.  We don't look for uniqueness
	for _BASE in "$@" ; do
		for _SUFFIX in ${_SUFFFIXES[@]} ; do
			_FILE="${_BASE}.${_SUFFIX}"
			[[ -e "$_FILE" ]] && _E_FLF+=( "$_FILE" )
		done
	done
}


# Source a local environment script.  E_HOME needs to be set.
# _senv_local $PROG
function _senv_local {
	typeset _PROG
	typeset _FILE
	[[ -n "$E_HOME" && -n "$E_USER" ]] || return
	_PROG=$1
	[[ -n "$_PROG" ]] || return
	# be very specific on the characters allowd.
	[[ "$_PROG" =~ ^[a-zA-z0-9_.-]+$ ]] || return
	_find_local_files "$_PROG"
	for _FILE in "${_E_FLF[@]}" ; do
		[[ -r $_FILE ]] || continue
		# Source it only if it's owned by E_USER
		[[ "`/usr/bin/stat --format='%U' $_efile 2>/dev/null`" = "$E_USER" ]] || continue
		. $_FILE
	done
}

# Pathmunge is used to prepend or append something to the path
# uniquely.  This is taken from a /etc/profile file, but it is sometimes
# removed by that file.
function pathmunge {
	case ":${PATH}:" in
		*:"$1":* ) : ;;
		* )
		if [ "$2" = "after" ] ; then
			PATH="${PATH}:${1}"
		else
			PATH="${1}:${PATH}"
		fi
		;;
	esac
}

# Search for an 'env.sh' file in the provided paths (current directory
# if none passed) and source it.
#
# senv [path1 path2 ...]
function senv {
	typeset SENVSTART
	typeset senvdir
	SENVSTART=`pwd` 
	for senvdir in ${@:-.}
	do
		case "$senvdir" in
			*/env.sh) senvdir=`dirname $senvdir`  ;;
			env.sh|./env.sh) senvdir=.  ;;
		esac
		[[ -f $senvdir/env.sh ]] || continue
		cd $senvdir || break
		. ./env.sh
		cd $SENVSTART
	done
	cd $SENVSTART
}

# Look for GSUXAUTH in the environment, and add that string to the
# xauth file.  This is used when doing a sudo.
function setxauth {
    if [[ -n "$GSUXAUTH" ]] && type -p xauth >/dev/null ; then
		echo "$GSUXAUTH" | xauth nmerge -  
		unset GSUXAUTH
    fi
}


##
## END FUNCTIONS
##
