# This file is provided by:
#
#   https://bitbucket.org/gramgersh/dotfiles/
#
# The basic profile under many linuxes source the .bashrc first, then set
# the PATH.  We will do it similarly.
if [ -z "$E_USER" ]; then
	[ -f $HOME/.erickson_user ] && read E_USER < $HOME/.erickson_user
	[ -z "$E_USER" ] && E_USER=erickson
fi

if [ -z "$E_HOME" ]; then
	[ -n "$E_USER" ] && E_HOME=$(getent passwd $E_USER | awk -F: '{ print $6 }')
fi

[[ -f "$E_HOME"/.bashrc ]] && . "$E_HOME"/.bashrc

for _p in /usr/sbin /sbin /usr/local/bin /usr/local/sbin $E_HOME/bin $E_HOME/bin.local $E_HOME/bin.$SHORTHOST $E_HOME/bin.$HOSTNAME $E_HOME/bin.$USER ; do
	[[ -d "$_p" && -x "$_p" ]] && pathmunge "$_p"
done
unset _p
SUDO_ARGS=""
sudo -V -E >/dev/null 2>&1 && SUDO_ARGS="-E"
export SUDO_ARGS

stty erase ^?
umask 022

if type -p keychain >/dev/null 2>&1 ; then
    case "`tty`" in
        /dev/pt[ys]* )
            keychain --ignore-missing --nocolor --nogui --quick --quiet id_rsa
            [[ -f $HOME/.keychain/$HOSTNAME-sh ]] && . $HOME/.keychain/$HOSTNAME-sh
            [[ -f $HOME/.keychain/$HOSTNAME-sh-gpg ]] && . $HOME/.keychain/$HOSTNAME-sh-gpg
            ;;
    esac
fi

# Do host- and site-specific startups
_senv_local .bash_profile
