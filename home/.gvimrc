set guifont=Courier\ 10\ Pitch\ 14
set lines=34
set columns=80
set guioptions=agimrLt
set tabstop=4
set shiftwidth=4
set laststatus=2
set background=light
colorscheme PaperColor
syntax on
set hls!
set compatible!
map! � <MiddleMouse>
