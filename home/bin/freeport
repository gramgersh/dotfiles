#!/usr/bin/perl
#
# This file is provided by:
#
#   https://bitbucket.org/gramgersh/dotfiles/
#
#
# $Id: freeport,v 1.1 2009/07/20 21:36:26 serickson Exp $
#
use strict;
use IO::File;
use Getopt::Long;

my $min = 3000;
my $max = 0xFFFF;
my $host = "127.0.0.1";

sub usage {
    my $xval = shift;
    @_ and print STDERR "@_\n";
    print STDERR "usage: $0 [--min port] [--max port] [--host ip]

Find an unused port.

   --min:   minimum port to use.  Default = 3000
   --max:  maximum port to use.  Default 65535
  --host:  ip address to use.  Default = '127.0.0.1'
";
    exit($xval);
}
GetOptions("min=i" => \$min,
	   "max=i" => \$max,
	   "host=s" => \$host,
	   "help" => sub { usage(0); },
	  )
  or usage(2);

if ( $host !~ m/^\d+\.\d+\.\d+\.\d+$/ ) {
    my ($name,$aliases,$addrtype,$length,@addrs) = gethostbyname($host);
    if ( ! @addrs ) {
	print STDERR "unknown host.\n";
	usage(1);
    }
    $host = join(".",unpack('C4',$addrs[0]));
}

my $fh = new IO::File "netstat -n --all --programs --numeric-ports 2>/dev/null|"
  or die "Cannot open 'netstat -n --all --programs --numeric-ports': $!\n";

my %used = ();
while ( <$fh> ) {
    my @x = split(/[\s:]+/,$_);
    next unless $x[3] eq $host;
    $used{$x[4]} = 1;
}
$fh->close();

while ( $used{$min} && $min < $max ) {
    $min++;
}
if ( $min >= $max ) {
    exit(1);
}
print "$min\n";
exit(0);
