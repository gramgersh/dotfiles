#! /usr/bin/perl
#
# This file is provided by:
#
#   https://bitbucket.org/gramgersh/dotfiles/
#
# cronmail - cron e-mail wrapper
#
use strict;
use warnings;
use Getopt::Long;
use Sys::Hostname;
use File::Temp qw/tempfile/;

my @addrs = ();
my $subj = undef;
my $uname = 0;
my $force = 0;
GetOptions(
		"addr=s@" => \@addrs,
		"to=s@" => \@addrs,
		"subj=s" => \$subj,
		"uname!" => \$uname,
		"hostname!" => \$uname,
		"force!" => \$force,
		"help|h" => sub {usage(0);}
		);

# Make the subject the command line if not provided.
defined($subj) or $subj = "@ARGV";
if ( $uname ) {
    my $hname = hostname;
	if ( $hname ) {
		$subj = "[$hname] $subj";
	}
}

# Create a temp file where the command output will be written
my ($outfh,$outfname) = tempfile('XXXXXXXX',UNLINK => 1);
$outfh->close();
chmod(0600,$outfname);

# Set out STDOUT and STDERR to the new file, then run the command
open(OLDSTDOUT,'>&',STDOUT);
open(OLDSTDERR,'>&',STDERR);
open(STDOUT, '>>', $outfname);
open(STDERR, '>>', $outfname);
system(@ARGV);
my $status = $?;

# Restore STDOUT and STDERR
open(STDOUT,'>&',OLDSTDOUT);
open(STDERR,'>&',OLDSTDERR);

# If the command ran fine (and we're not forcing outout), just exit
# nicely.
if ( $status >> 8 == 0 && ! $force ) {
	exit(0);
}

if ( $status == -1 ) {
	# failed to execute
	print "Could not run command: @_\n$!\n";
	exit(1);
}

# At this point, we either died with a signal, or we're forcing email
# to be sent.

# If no addresses were specified, see if the crontab set an address.
if ( ! @addrs ) {
	if ( exists($::ENV{MAILTO}) && length($::ENV{MAILTO}) ) {
		push(@addrs,$::ENV{MAILTO});
	}
	else {
		push(@addrs, scalar getpwuid($<));
	}
}

# Grab the data from the output file.
my $body = undef;
my $fh;
if ( ! open($fh,$outfname) ) {
	$body = "Unable to command output.";
}
else {
	# Read up to 4K
	read ($fh,$body,4096);
	close($fh);
}

my $header = "";
$header .= "To: " . join(",",@addrs) . "\n";
$header .= "Subject: $subj\n";
$header .= "\n";

# See which mail program we can use
if ( -x '/usr/sbin/sendmail' ) {
	if ( open(SENDMAIL,'|/usr/sbin/sendmail -t') ) {
		print SENDMAIL $header, $body;
		close(SENDMAIL);
		exit(0);
	}
}
if ( -x '/usr/bin/mailx' || -x '/bin/mail' ) {
	my $mailprog = '/bin/mail ';
	-x '/usr/bin/mailx' and $mailprog = '/usr/bin/mailx ';

	# Quote the quotes in the subject
	$subj =~ s/'/\\'/g;

	$mailprog .= "-s '$subj' @addrs";
	if ( open(MAILPROG,"|$mailprog") ) {
		print MAILPROG $body;
		close(MAILPROG);
		exit(0);
	}
}

# We couldn't find a mail program.  Just print out the data
print $body, "\n";
exit(1);
